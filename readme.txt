Multicast app.

Dependencies
============
Tomcat 8
JDK 1.8
The only extra library pulled in is junit 4

Demo steps
===========

1. Start instance1
Expected:
Instance 1 fetches configuration from db
Instance 1 writes to serverinfo

2. Instance 1 updates configuration
Expected:
Configuration DB updates.
No notifications

3. Start instance 2
Instance 2 fetches configuration from db, should match what was updated by instance 1
Instance 2 writes serverinfo
Instance 2 notifies instance 1 of new server
Instance 1 fetches serverinfo

4. Start instance 3
Instance 3 fetches configuration from db, should match what was updated by instance 1
Instance 3 writes serverinfo
Instance 3 notifies instance 1 and 2 of new server
Instance 1,2 fetch serverinfo

5. Instance 2 updates configuration
Expected:
Configuration DB updates.
Instance 2 notifies instance 1,3 of new configuration.
Instance 1,3 refetch configurations from DB

6. At this point, instance 1 and 3's page is out of sync between what user is seeing
   and what's cached.  Instance 1 update configuration.
Expected:
Instance 1 will provide a visual warning.
Hit force update to update to server
Instance 1 notifies instance 2,3 of new configuration.
Instance 2,3 refetch configurations from DB

7. Instance 1 leaves the cluster in a catastrophic failure. so instance 2,3 still thinks there are 3 nodes.
Instance 2 updates configuration.
Expected:
Instance 2 will notify instance 1,3 about new configuration.
Instance 3 will refetch new server infos.
Instance 2 will realize instance 1 is down
Instance 2 updates server info DB
Instance 2 notifies instance 3 about new server infos.
Instance 3 refetches. server infos.

8 Instance 3 leaves the cluster peacefully.
Expected:
Server Info DB updates.
Instance 3 notifies instance 2
Instance 2 refetch server infos from DB

9. Instance 2 updates configuration
Expected:
Configuration DB updates.
No notifications will be sent.




