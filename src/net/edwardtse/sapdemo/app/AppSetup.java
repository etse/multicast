package net.edwardtse.sapdemo.app;

import net.edwardtse.sapdemo.domain.ServerInfo;

import javax.management.*;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.Set;

/**
 * To handle tasks that are related to the lifecycle of the server, such as startup and tear down.
 * Created by etse on 12/3/16.
 */
public class AppSetup {
  /**
   * Server startup initization.
   */
  public static void bootstrap() {

    try {
      ApplicationContext ctx = ApplicationContext.getInstance();

      // Add itself to the server list first.
      ServerInfo currentServerInfo = getCurrentServerInfo();
      ctx.setCurrentServerInfo(currentServerInfo);

      // Load configuration from DB into cache.
      ctx.setCachedConfiguration(ctx.getConfigurationDAO().getConfiguration());

      ctx.setCachedServerInfos(ctx.getServerInfoDAO().findAll());

      ctx.getServerInfoService().save(currentServerInfo);

    } catch (Exception e) {
      e.printStackTrace();
      System.exit(1);
    }
  }

  /**
   * Server tear down
   */
  public static void destroy() {
    ApplicationContext ctx = ApplicationContext.getInstance();
    ctx.getServerInfoService().removeCurrentServer();

  }

  /**
   * Just to get host and ports, this code looks messy but it's only purpose is to get host and port
   *
   */
  private static ServerInfo getCurrentServerInfo() throws MalformedObjectNameException,
      NullPointerException, UnknownHostException, AttributeNotFoundException,
      InstanceNotFoundException, MBeanException, ReflectionException {
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    Set<ObjectName> objs = mbs.queryNames(new ObjectName("*:type=Connector,*"),
        Query.match(Query.attr("protocol"), Query.value("HTTP/1.1")));
    String hostname = InetAddress.getLocalHost().getHostName();
    InetAddress[] addresses = InetAddress.getAllByName(hostname);

    for (Iterator<ObjectName> i = objs.iterator(); i.hasNext(); ) {
      ObjectName obj = i.next();
      String scheme = mbs.getAttribute(obj, "scheme").toString();
      String port = obj.getKeyProperty("port");
      for (InetAddress addr : addresses) {

        // only interest in ipv4 for now


        String host = addr.getHostAddress();
        return new ServerInfo(host, port);
      }
    }

    return null;
  }
}
