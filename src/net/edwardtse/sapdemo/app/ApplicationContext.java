package net.edwardtse.sapdemo.app;

import net.edwardtse.sapdemo.dao.IConfigurationDAO;
import net.edwardtse.sapdemo.dao.IServerInfoDAO;
import net.edwardtse.sapdemo.domain.Configuration;
import net.edwardtse.sapdemo.dao.ConfigurationDAOFileImpl;
import net.edwardtse.sapdemo.domain.ServerInfo;
import net.edwardtse.sapdemo.dao.ServerInfoDAOFileImpl;
import net.edwardtse.sapdemo.service.ConfigurationService;
import net.edwardtse.sapdemo.service.ServerInfoService;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * A Class to store application context during runtime.
 *
 * Created by etse on 12/3/16.
 */
public class ApplicationContext {

  private static ApplicationContext ctx;
  private Configuration cachedConfiguration;
  private List<ServerInfo> cachedServerInfos;
  private IConfigurationDAO configurationDAO;
  private IServerInfoDAO serverInfoDAO;
  private ConfigurationService configurationService;
  private ServerInfoService serverInfoService;

  private ServerInfo currentServerInfo;
  private ExecutorService executorService;


  public ApplicationContext() {
    configurationDAO = new ConfigurationDAOFileImpl("./data/configuration.txt");
    serverInfoDAO = new ServerInfoDAOFileImpl("./data/serverInfos.txt");

    configurationService = new ConfigurationService();
    configurationService.setApplicationContext(this);
    configurationService.setConfigurationDAO(configurationDAO);

    // Setup
    serverInfoService = new ServerInfoService();
    serverInfoService.setApplicationContext(this);
    serverInfoService.setServerInfoDAO(serverInfoDAO);

    //* setup the thread pool to 5 for notification.
    executorService = Executors.newFixedThreadPool(5);
  }

  public static ApplicationContext getInstance() {

    if (ctx == null) {
      synchronized (ApplicationContext.class) {
        if (ctx == null) {
          ctx = new ApplicationContext();
        }
      }
    }
    return ctx;
  }


  public IConfigurationDAO getConigurationDAO() {
    return configurationDAO;
  }

  public Configuration getCachedConfiguration() {
    return cachedConfiguration;
  }

  public IServerInfoDAO getServerInfoDAO() {
    return serverInfoDAO;
  }

  public ConfigurationService getConfigurationService() {
    return this.configurationService;
  }

  public IConfigurationDAO getConfigurationDAO() {
    return configurationDAO;
  }

  public List<ServerInfo> getCachedServerInfos() {
    return cachedServerInfos;
  }

  public void setCachedConfiguration(Configuration cachedConfiguration) {
    this.cachedConfiguration = cachedConfiguration;
  }

  public void setCachedServerInfos(List<ServerInfo> cachedServerInfos) {
    this.cachedServerInfos = cachedServerInfos;
  }

  public ServerInfo getCurrentServerInfo() {
    return currentServerInfo;
  }

  public void setCurrentServerInfo(ServerInfo currentServerInfo) {
    this.currentServerInfo = currentServerInfo;
  }

  public ServerInfoService getServerInfoService() {
    return serverInfoService;
  }

  public ExecutorService getExecutorService() {
    return executorService;
  }
}
