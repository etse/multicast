package net.edwardtse.sapdemo.domain;

/**
 *
 * ServerInfo domain object.
 *
 * Created by etse on 12/3/16.
 */
public class ServerInfo {

  public ServerInfo(String host, String port) {
    this.host = host;
    this.port = port;
  }

  public ServerInfo(){

  }

  private String host;
  private String port;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof ServerInfo)) return false;

    ServerInfo that = (ServerInfo) o;

    if (host != null ? !host.equals(that.host) : that.host != null) return false;
    return port != null ? port.equals(that.port) : that.port == null;

  }

  @Override
  public int hashCode() {
    int result = host != null ? host.hashCode() : 0;
    result = 31 * result + (port != null ? port.hashCode() : 0);
    return result;
  }

  public String getHost() {
    return host;
  }

  public void setHost(String host) {
    this.host = host;
  }

  public String getPort() {
    return port;
  }

  public void setPort(String port) {
    this.port = port;
  }

  @Override
  public String toString() {
    return host + ":" + port;
  }
}
