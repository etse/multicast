package net.edwardtse.sapdemo.domain;

/**
 *
 * Configuration domain object.
 *
 * Created by etse on 12/3/16.
 */
public class Configuration {

  private String content;

  public Configuration(String content) {
    this.content = content;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String value) {
    this.content = value;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Configuration)) return false;

    Configuration that = (Configuration) o;

    return content != null ? content.equals(that.content) : that.content == null;

  }

  @Override
  public int hashCode() {
    return content != null ? content.hashCode() : 0;
  }
}
