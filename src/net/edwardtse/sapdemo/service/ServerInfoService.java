package net.edwardtse.sapdemo.service;

import net.edwardtse.sapdemo.app.ApplicationContext;
import net.edwardtse.sapdemo.dao.IServerInfoDAO;
import net.edwardtse.sapdemo.domain.ServerInfo;

import java.util.List;

/**
 * Handle operational logic for ServerInfo
 *
 * Created by etse on 12/3/16.
 */
public class ServerInfoService {


  IServerInfoDAO serverInfoDAO;
  private ApplicationContext ctx;

  public void save(ServerInfo currentServerInfo) {

    synchronized (this){
      if (serverInfoDAO.save(ctx.getCachedServerInfos(), currentServerInfo)) {
        ctx.setCachedServerInfos(ctx.getServerInfoDAO().findAll());
        notifyServerInfoUpdate();
      }
    }
  }

  private void notifyServerInfoUpdate() {

    final List<ServerInfo> cachedServerInfos = ctx.getCachedServerInfos();
    for (ServerInfo serverInfo : cachedServerInfos) {

      if (!serverInfo.equals(ctx.getCurrentServerInfo())) {

        Runnable serverUpdateTask = new ServerUpdateRunnable(serverInfo, "/serverInfo/refetchCache", "update server infos");
        ctx.getExecutorService().execute(serverUpdateTask);
      }
    }
  }


  public void setServerInfoDAO(IServerInfoDAO serverInfoDAO) {
    this.serverInfoDAO = serverInfoDAO;
  }

  public void setApplicationContext(ApplicationContext ctx) {
    this.ctx = ctx;
  }

  public void refetchCache() {
    ctx.setCachedServerInfos(serverInfoDAO.findAll());
  }

  public void removeCurrentServer() {
    remove(ctx.getCurrentServerInfo());
  }

  public void remove(ServerInfo serverInfo) {
    IServerInfoDAO serverInfoDAO = ctx.getServerInfoDAO();
    ctx.getCachedServerInfos().remove(serverInfo);
    serverInfoDAO.delete(serverInfo);
    notifyServerInfoUpdate();
  }
}
