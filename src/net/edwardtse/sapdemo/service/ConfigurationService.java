package net.edwardtse.sapdemo.service;

import net.edwardtse.sapdemo.app.ApplicationContext;
import net.edwardtse.sapdemo.dao.IConfigurationDAO;
import net.edwardtse.sapdemo.domain.Configuration;
import net.edwardtse.sapdemo.domain.ServerInfo;

import java.util.List;

/**
 *
 * Handle operational logic for Configuration
 *
 * Created by etse on 12/3/16.
 */
public class ConfigurationService {


  private ApplicationContext ctx;

  private IConfigurationDAO configurationDAO;

  public void refetchCache() {
    Configuration config = configurationDAO.getConfiguration();
    ApplicationContext.getInstance().setCachedConfiguration(config);
  }

  private void notifyConfigurationUpdate() {

    final List<ServerInfo> cachedServerInfos = ctx.getCachedServerInfos();
    for (ServerInfo serverInfo : cachedServerInfos) {

      if (!serverInfo.equals(ctx.getCurrentServerInfo())) {
        Runnable serverUpdateTask = new ServerUpdateRunnable(serverInfo, "/configuration/refetchCache", "update configuration");
        ctx.getExecutorService().execute(serverUpdateTask);
      }
    }
  }

  public void saveContent(String configurationContent) {
    Configuration c = new Configuration(configurationContent);
    synchronized (this) {
      ctx.setCachedConfiguration(c);
      configurationDAO.save(c);
    }
    notifyConfigurationUpdate();
  }




  public IConfigurationDAO getConfigurationDAO() {
    return configurationDAO;
  }

  public void setApplicationContext(ApplicationContext ctx) {
      this.ctx = ctx;
    }

  public void setConfigurationDAO(IConfigurationDAO configurationDAO) {
    this.configurationDAO = configurationDAO;
  }


}
