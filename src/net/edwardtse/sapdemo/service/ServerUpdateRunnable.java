package net.edwardtse.sapdemo.service;

import net.edwardtse.sapdemo.app.ApplicationContext;
import net.edwardtse.sapdemo.domain.ServerInfo;
import net.edwardtse.sapdemo.util.HttpUtil;
import net.edwardtse.sapdemo.util.LogUtil;

import java.io.IOException;

/**
 * A Task to notify other servers in the cluster in the event of changes.
 *
 * Created by etse on 12/4/16.
 */
public class ServerUpdateRunnable implements Runnable {

  private ServerInfo serverInfo;
  private String path;
  private String msg;

  public  ServerUpdateRunnable (ServerInfo serverInfo, String path, String msg) {
    this.serverInfo = serverInfo;
    this.path = path;
    this.msg = msg;
  }

  @Override
  public void run() {

    try {
      LogUtil.log("Notify " + serverInfo.getHost()+ ":" + serverInfo.getPort() + " ~ " + msg);
      HttpUtil.send("http://" + serverInfo.getHost() + ":" + serverInfo.getPort() + path, "PUT");
    } catch (IOException e) {

      // In the case of an IOException, we will remove the server from the server list.
      ServerInfoService serverInfoService = ApplicationContext.getInstance().getServerInfoService();
      serverInfoService.remove(serverInfo);
    }
  }
}
