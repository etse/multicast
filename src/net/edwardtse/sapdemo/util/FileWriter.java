package net.edwardtse.sapdemo.util;


import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * This is to help writing content to file.
 *
 * Created by etse on 12/3/16.
 */
public class FileWriter {

  private static final int RETRY = 3;
  private static final int RETRY_DELAY = 500; // milli seconds.

  private Path path;

  public FileWriter(String filename) {
    path = Paths.get(filename);
    System.out.println("DB file: path.toFile().getAbsolutePath()");

    // Check directory exist
    File parent = path.toFile().getParentFile();
    if (!parent.exists()) {
      parent.mkdirs();
    }

  }

  /**
   * Get content from a file to string
   *
   * @return Content as a string.
   * @throws IOException When the method failed to read and has maxed out the number of retries.
   */
  public String getContent() throws IOException {

    int tryCount = 0;
    while (tryCount < RETRY ) {
      try {
        if (path.toFile().exists()) {
          String content = new String(Files.readAllBytes(path));
          return content;
        } else {
          return null;
        }
      } catch (IOException i) {
        tryCount++;
        try {
          Thread.sleep(RETRY_DELAY);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
    throw new IOException("Could not read from " + path.toFile().getAbsolutePath());
  }

  /**
   * Get content from a file by putting each line into String List.
   *
   * @return A String List representing the lines in the file.
   * @throws IOException IOException When the method failed to read and has maxed out the number of retries.
   */
  public List<String> getContentLines() throws IOException {
    if (path.toFile().exists()) {
      List<String> list = Files.readAllLines(path, Charset.defaultCharset());
      return list;
    } else {
      return null;
    }
  }

  /**
   * Save a string to a file.
   * @param content The content to e saved.
   * @throws IOException IOException When the method failed to write  and has maxed out the number of retries.
   */
  public void setContent(String content) throws IOException {
    int tryCount = 0;
    while (tryCount < RETRY) {
      try {

        Files.write(path, content.getBytes());
        return;
      } catch (IOException i ) {
        tryCount++;
        try {
          Thread.sleep(RETRY_DELAY);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
    throw new IOException("Could not write  to " + path.toFile().getAbsolutePath());
  }
}
