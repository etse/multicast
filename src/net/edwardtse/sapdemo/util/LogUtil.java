package net.edwardtse.sapdemo.util;

import net.edwardtse.sapdemo.app.ApplicationContext;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * A Utility to help centralize where console or log output go.
 *
 *
 * Created by etse on 12/3/16.
 */
public class LogUtil {

  public static void log(String msg) {
//    try {

      msg = ApplicationContext.getInstance().getCurrentServerInfo().toString() + ": " + msg + "\n";
      //Files.write(Paths.get("/tmp/output.txt"), msg.getBytes(), StandardOpenOption.APPEND);
      System.out.println(msg);
//    } catch (IOException e) {
//      e.printStackTrace();
//      //exception handling left as an exercise for the reader
//    }
  }
}
