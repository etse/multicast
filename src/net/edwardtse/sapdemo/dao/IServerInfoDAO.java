package net.edwardtse.sapdemo.dao;

import net.edwardtse.sapdemo.domain.ServerInfo;

import java.util.List;

/**
 * Created by etse on 12/4/16.
 */
public interface IServerInfoDAO {

  /**
   * Find all the ServerInfos
   * @return
   */
  List<ServerInfo> findAll();

  /**
   * Save the specified ServerInfo into db.
   *
   * @param serverInfoToBeSaved
   * @return a boolean, true if it is dirtry and wrote to the database.
   */
  boolean save(ServerInfo serverInfoToBeSaved);

  /**
   * Save a server into the given list.
   * @param serverInfos
   * @param serverInfoToBeSaved
   * @return
   */
  boolean save(List<ServerInfo> serverInfos, ServerInfo serverInfoToBeSaved);

  /**
   * Delete ServerInfo
   *
   * @param serverInfoToBeDeleted The server info to be deleted.
   */
  void delete(ServerInfo serverInfoToBeDeleted);
}
