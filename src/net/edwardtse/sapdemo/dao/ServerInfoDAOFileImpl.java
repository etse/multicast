package net.edwardtse.sapdemo.dao;

import net.edwardtse.sapdemo.util.FileWriter;
import net.edwardtse.sapdemo.domain.ServerInfo;
import net.edwardtse.sapdemo.util.LogUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Facility to handle ServerInfo data objects.
 *
 * Created by etse on 12/3/16.
 */
public class ServerInfoDAOFileImpl implements IServerInfoDAO {

  private FileWriter db;

  public ServerInfoDAOFileImpl(String file) {
    db = new FileWriter(file);
  }

  @Override
  public List<ServerInfo> findAll() {
    try {

      List<String> lines = db.getContentLines();

      if (lines != null) {
        LogUtil.log("Fetch ServerInfo list from DB ~ " + lines.size() + " servers");
        return lines.stream().map(s -> strToServerInfo(s)).collect(Collectors.toList());
      } else {
        LogUtil.log("Fetch ServerInfo list from DB ~ empty");
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return new ArrayList<>();
  }

  /**
   * Save all the server infos.
   *
   * @param serverInfos the ServerInfos to be saved.
   * @return whether a save operation has been performed.
   * @throws IOException
   */
  private boolean save(List<ServerInfo> serverInfos) throws IOException {
    if (serverInfos != null) {

      String content = serverInfos.stream().map(e -> e.toString()).collect(Collectors.joining("\r\n"));
      db.setContent(content);
      LogUtil.log("Update ServerInfo list to DB");
      return true;

    }
    return false;
  }

  private static ServerInfo strToServerInfo(String line) {
    String[] parts = line.split(":");
    return new ServerInfo(parts[0], parts[1]);
  }

  /**
   * Save the specified ServerInfo into db.
   *
   * @param serverInfoToBeSaved
   * @return a boolean, true if it is dirtry and wrote to the database.
   */
  @Override
  public boolean save(ServerInfo serverInfoToBeSaved) {
    if (serverInfoToBeSaved != null) {

      List<ServerInfo> serverInfos = findAll();

      if (!serverInfos.stream().filter(x -> serverInfoToBeSaved.equals(x) ).findAny().isPresent()) {

        serverInfos.add(serverInfoToBeSaved);
        try {
          save(serverInfos);
          return true;
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    return false;
  }

  /**
   * Save a server into the given list.
   * @param serverInfos
   * @param serverInfoToBeSaved
   * @return
   */
  @Override
  public boolean save(List<ServerInfo> serverInfos, ServerInfo serverInfoToBeSaved) {
    if (serverInfoToBeSaved != null) {

      if (!serverInfos.stream().filter(x -> serverInfoToBeSaved.equals(x)).findAny().isPresent()) {

        serverInfos.add(serverInfoToBeSaved);
        try {
          save(serverInfos);
          return true;
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    return false;
  }

  /**
   * Delete ServerInfo
   *
   * @param serverInfoToBeDeleted The server info to be deleted.
   */
  @Override
  public void delete(ServerInfo serverInfoToBeDeleted) {
    List<ServerInfo> serverInfos = findAll();

    Optional<ServerInfo> s = serverInfos.stream().filter(x -> serverInfoToBeDeleted.equals(x)).findAny();

    if (s.isPresent()) {

      serverInfos.remove(s.get());
      try {
        save(serverInfos);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
