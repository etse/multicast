package net.edwardtse.sapdemo.dao;

import net.edwardtse.sapdemo.domain.Configuration;

/**
 * Created by etse on 12/4/16.
 */
public interface IConfigurationDAO {

  /**
   * Get the configuration
   *
   * @return the configuration.
   */
  Configuration getConfiguration();

  /**
   * Save the configuration.
   * @param configuration the configuration to be saved.
   */
  void save(Configuration configuration);
}
