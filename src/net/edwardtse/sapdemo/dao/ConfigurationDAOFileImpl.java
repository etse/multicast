package net.edwardtse.sapdemo.dao;

import net.edwardtse.sapdemo.util.FileWriter;
import net.edwardtse.sapdemo.domain.Configuration;
import net.edwardtse.sapdemo.util.LogUtil;

import java.io.IOException;

/**
 *
 * Facility to handle Configuration data objects.
 *
 * Created by etse on 12/3/16.
 */
public class ConfigurationDAOFileImpl implements IConfigurationDAO {
  private FileWriter db;

  public ConfigurationDAOFileImpl(String file) {
    db= new FileWriter(file);
  }

  @Override
  public Configuration getConfiguration() {
    try {
      LogUtil.log("Load configuration from DB");
      return new Configuration(db.getContent());
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }


  @Override
  public void save(Configuration configuration) {
    try {
      LogUtil.log("Save configuration to DB");
      db.setContent(configuration.getContent());
    } catch (IOException e) {
      e.printStackTrace();
    }
  }


}
