package net.edwardtse.sapdemo.servlet.helper;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Helper for constructing a HTTP request success message..
 * Created by etse on 12/3/16.
 */
public class ServletUtil {
  public static void respondJsonSuccess(HttpServletResponse resp) throws IOException {

    resp.setContentType("application/json");
    // Get the printwriter object from response to write the required json object to the output stream
    PrintWriter out = resp.getWriter();
    //out.print("{\"Status\":\"Success\"}");
    out.flush();
  }
}
