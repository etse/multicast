package net.edwardtse.sapdemo.servlet;

import net.edwardtse.sapdemo.app.ApplicationContext;
import net.edwardtse.sapdemo.service.ServerInfoService;
import net.edwardtse.sapdemo.servlet.helper.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by etse on 12/3/16.
 */
@WebServlet("/serverInfo/refetchCache")
public class ServerInfoServlet extends HttpServlet {

  ServerInfoService serverInfoService = ApplicationContext.getInstance().getServerInfoService();

  @Override
  protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    serverInfoService.refetchCache();
    ServletUtil.respondJsonSuccess(resp);

  }
}
