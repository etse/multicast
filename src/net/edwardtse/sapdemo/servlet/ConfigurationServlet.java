package net.edwardtse.sapdemo.servlet;

import net.edwardtse.sapdemo.app.ApplicationContext;
import net.edwardtse.sapdemo.app.AppSetup;
import net.edwardtse.sapdemo.domain.Configuration;
import net.edwardtse.sapdemo.service.ConfigurationService;
import net.edwardtse.sapdemo.servlet.helper.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by etse on 12/3/16.
 */

@WebServlet(value = {"/configuration/*", ""}, loadOnStartup = 1)
public class ConfigurationServlet extends HttpServlet {

  ApplicationContext ctx = ApplicationContext.getInstance();
  ConfigurationService configurationService = ctx.getConfigurationService();
  Configuration lastConfigurationShown;

  @Override
  public void init() throws ServletException {
    super.init();
    AppSetup.bootstrap();
  }

  @Override
  public void destroy() {
    super.destroy();
    AppSetup.destroy();
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    lastConfigurationShown = ctx.getCachedConfiguration();
    String configurationContent = lastConfigurationShown.getContent();
    if (configurationContent == null) {
      configurationContent = "";
    }
    dispatch(req, resp, configurationContent);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String configurationContent = req.getParameter("configurationContent");


    // Either this is a force svae or we need to ensure shown is still the same as the one in cache,
    // or we have to warn user.
    if (Boolean.parseBoolean(req.getParameter("forceUpdate")) || lastConfigurationShown.equals(ctx.getCachedConfiguration())) {
      // Since nothing have changed, we can continue.

      // Check to see if what is shown is what is being saved, if they are the same, skip it.
      if (configurationContent != null && !configurationContent.equals(lastConfigurationShown.getContent())) {
        configurationService.saveContent(configurationContent);

      }
    } else {
      req.setAttribute("saveWarning", true);
    }

    dispatch(req, resp, configurationContent);
  }

  /**
   * PUT /configuration/updateCache
   */
  @Override
  protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    String pathInfo = req.getPathInfo();
    if (pathInfo.equals("/refetchCache")) {
      configurationService.refetchCache();
      ServletUtil.respondJsonSuccess(resp);
    } else {
      resp.sendError(HttpServletResponse.SC_NOT_FOUND);
    }
  }

  private void dispatch(HttpServletRequest req, HttpServletResponse resp, String configurationContent) throws ServletException, IOException {
    req.setAttribute("configurationContent", configurationContent);
    req.setAttribute("serverInfo", ctx.getCurrentServerInfo());
    req.getRequestDispatcher("/config.jsp").forward(req, resp);
  }
}
