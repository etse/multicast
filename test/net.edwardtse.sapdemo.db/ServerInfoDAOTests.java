package net.edwardtse.sapdemo.db;

import net.edwardtse.sapdemo.dao.IServerInfoDAO;
import net.edwardtse.sapdemo.domain.ServerInfo;
import net.edwardtse.sapdemo.dao.ServerInfoDAOFileImpl;
import org.junit.Test;


import java.io.File;

import static org.junit.Assert.assertTrue;

/**
 * Created by etse on 12/3/16.
 */
public class ServerInfoDAOTests {


  @Test
  public void testCRUD() {

    File f = new File("./test/tmp/serverinfos.txt");
    if (f.exists()) {
      f.delete();
    }
    IServerInfoDAO serverInfoDAO = new ServerInfoDAOFileImpl("./test/tmp/serverinfos.txt");
    assertTrue(serverInfoDAO.findAll().size() == 0);

    // Test save
    serverInfoDAO.save(new ServerInfo("localhost", "8080"));
    assertTrue(serverInfoDAO.findAll().size() == 1);

    serverInfoDAO.save(new ServerInfo("localhost", "8080"));
    assertTrue(serverInfoDAO.findAll().size() == 1);

    ServerInfo serverInstance2 = new ServerInfo("localhost", "8090");
    serverInfoDAO.save(serverInstance2);
    assertTrue(serverInfoDAO.findAll().size() == 2);

    serverInfoDAO.delete(serverInstance2);
    assertTrue(serverInfoDAO.findAll().size() == 1);
  }
}
