package net.edwardtse.sapdemo.db;

import net.edwardtse.sapdemo.util.FileWriter;
import net.edwardtse.sapdemo.util.HttpUtil;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by etse on 12/3/16.
 */
public class FileUtilTests {


  @Test
  public void testReadWrite() {


    String testFile = "./test/tmp/testfile.txt";

    // Given a db file not exists
    File f = new File(testFile);
    if (f.exists()) {
      f.delete();
    }

    // Expect reading from the file would return null
    FileWriter fileDb = new FileWriter(testFile);
    try {
      assertNull("File does not exist should return null", fileDb.getContent());

      // Then write something to file
      String testMsg = "adjksdfaj";
      fileDb.setContent(testMsg);

      // Expect to read the same back
      assertEquals(testMsg, fileDb.getContent());

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
