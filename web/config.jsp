<%--
  Created by IntelliJ IDEA.
  User: etse
  Date: 12/3/16
  Time: 12:09 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Configuration </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container-fluid">
    <h1>Configuration ${serverInfo}</h1>
    <div class="form-group">

        <form action="/configuration" method="post">
            <textarea class="form-control" rows="20" id="configurationContent"
                      name="configurationContent">${configurationContent}</textarea>

            <%
                Boolean saveWarning = (Boolean) request.getAttribute("saveWarning");
                if (saveWarning != null && saveWarning == Boolean.TRUE) {
            %>
            <input type="hidden" name="forceUpdate" value="true"/>

            <div class="alert alert-danger">
                <strong>Update skipped.</strong>  The configuration was modified by someone else before the save.  Hit 'Refresh' to load an updated copy or hit 'Force Update' to ignore this warning and update.
            </div>
            <button type="submit" class="btn btn-danger">Force Update</button>
            <a href="/configuration" class="btn btn-success">Refresh</a>
            <br/>
            <% } else { %>
            <button type="submit" class="btn btn-success">Update</button>
            <a href="/configuration" class="btn btn-success">Refresh</a>
            <br/>
            <% } %>

        </form>

    </div>
</div>
</body>
</html>
